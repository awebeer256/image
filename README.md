# Minimal Docker for LOS

Build LineageOS + microG (optional) from a minimal Docker container.

[[_TOC_]]

## Getting started

> You can run your own build on DigitalOcean with [Infra](https://gitlab.com/fremontt/minimal-docker-for-lineageos/infra), just **[create a new account and get $100 USD free in credits](https://m.do.co/c/397805aa0e0e)**

In this guide, `$YOUR_PROJECT_FOLDER` refers to a location on your computer (or cloud instance) where all the needed build files will be stored (more than 200GB), including your signature keys and output builds.

## Setup

1. Make sure you have at least 250GB free in `$YOUR_PROJECT_FOLDER`, then create some folders inside it:
```
cd $YOUR_PROJECT_FOLDER && mkdir -p {ccache,custom_keys,custom_manifests}
```
3. Copy this XML to `$YOUR_PROJECT_FOLDER/custom_manifests/minimal-docker-los.xml`, changing the vendor text to match your device.
```
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
    <remote name="themuppetsGithub" fetch="https://github.com/TheMuppets/" revision="lineage-17.1" />
    <remote name="themuppetsGitlab" fetch="https://gitlab.com/the-muppets/" revision="lineage-17.1" />
    <remote name="omniromGithub" fetch="https://github.com/omnirom/" />
    <remote name="minimalLOSGitlab" fetch="https://gitlab.com/fremontt/minimal-docker-for-lineageos/" />

    <project name="android_prebuilts_prebuiltapks" path="packages/apps/prebuilt" remote="omniromGithub" revision="android-10" />
    <project name="android_sigspoof_patches" path="patches" remote="minimalLOSGitlab" revision="lineage-17.1" />

    <project name="proprietary_vendor_motorola" path="vendor/motorola" remote="themuppetsGithub" />
</manifest>
```
> The list of vendors can be found [here](https://github.com/TheMuppets/manifests/blob/lineage-17.1/muppets.xml).

> This XML is a Manifest file to define all additional sources to build LOS with microG and your vendor/device-specific binaries. You can find more information about the format of this file [here](https://gerrit.googlesource.com/git-repo/+/master/docs/manifest-format.md).

> LPT: Avoid putting comments into the manifest file or you will suffer a hell of random errors.

3. Generate your signature keys, using [this code snippet](https://wiki.lineageos.org/signing_builds.html#generating-the-keys) as a guide. The `make_key` script it references can be found [here](https://github.com/LineageOS/android_development/blob/lineage-17.1/tools/make_key). Copy all the key files into `$YOUR_PROJECT_FOLDER/custom_keys`.

> You only need to generate your build keys once; on later builds simply copy your existing files to the folder. If you lose your keys, you can generate new ones, but you will need to do a clean install of the new build (not an update).

> It doesn't really matter what information you put into the subject string, but that string is part of the data being signed by your keys, and `make_key` will fail if you leave any of the subject's sections blank, so you might as well fill it with meaningful, correct information. CN should be your first and last name; the remaining fields are named in the picture embedded [here](https://developer.android.com/studio/publish/app-signing#generate-key). If you want to change the subject string later, you will have to generate new keys.

## Running build

Running the build should be as easy as executing a couple of commands.

1. Build the Docker image: `docker build -t fremontt/losbuilder <path or url to this repository>`

2. Run a new Docker container that will build your LOS image automatically: `docker run -iv $YOUR_PROJECT_FOLDER:/root/los-build fremontt/losbuilder`

3. When the build finishes, you should check the zip files at `$YOUR_PROJECT_FOLDER/lineage-*.zip`

**Please read the examples before doing anything.**

### Examples

Remember to ALWAYS build the Docker image before running it. In these examples I assume you have already run the command (1) and understand the basics of Docker.

#### Building for Moto G7 Plus (lake): default of this project

`docker run -iv $YOUR_PROJECT_FOLDER:/root/los-build fremontt/losbuilder`

#### Building for lake with the full log output on a file

`docker run -iv $YOUR_PROJECT_FOLDER:/root/los-build fremontt/losbuilder 2>&1 | tee /home/<your-username>/los-microg-build.log`

#### Building for lake while leaving one CPU core free (so your system is still responsive in the meantime)

`docker run -iv $YOUR_PROJECT_FOLDER:/root/los-build --cpuset-cpus "0-$(( ``nproc`` - 2 ))" fremontt/losbuilder`

> Yes, that's meant to be a 2, not a 1. It's an [off-by-one](https://en.wikipedia.org/wiki/Off-by-one_error) thing.

> If you have more than four cores, you might want to leave more than one free, e.g. -3 to leave two free on an eight-core system.

#### Building for lake + FULL signature spoofing (all installed apps could have the sigspoof permission)

`docker run -iv $YOUR_PROJECT_FOLDER:/root/los-build -e LOS_SIGSPOOF_TYPE="FULL" fremontt/losbuilder`

#### Building for lake + RESTRICTED signature spoofing (only system apps can have the sigspoof permission) (Official LOS4μG builds have restriced spoofing)

`docker run -iv $YOUR_PROJECT_FOLDER:/root/los-build -e LOS_SIGSPOOF_TYPE="RESTRICTED" fremontt/losbuilder`

#### Building for lake + RESTRICTED signature spoofing + [custom packages](https://github.com/omnirom/android_prebuilts_prebuiltapks)

`docker run -iv $YOUR_PROJECT_FOLDER:/root/los-build -e LOS_SIGSPOOF_TYPE="RESTRICTED" -e LOS_ENABLE_CUSTOM_PACKAGES="YES" -e LOS_CUSTOM_PACKAGES="GmsCore GsfProxy FakeStore MozillaNlpBackend com.google.android.maps" fremontt/losbuilder`

#### Building for another supported device: [Xiaomi "lavender"](https://wiki.lineageos.org/devices/lavender/)

> Note: I haven't tested this because I'm not a "lavender" device owner; is just an example.

**Manifest file**

```
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
    ...
    <project name="proprietary_vendor_xiaomi" path="vendor/xiaomi" remote="themuppetsGitlab" />
    ...
</manifest>
```

`docker run -iv $YOUR_PROJECT_FOLDER:/root/los-build -e DEVICE_VENDOR="xiaomi" -e DEVICE_CODENAME="lavender" fremontt/losbuilder`

## FAQ

### Does this build have UnifiedNLP included?

No. I will need your help to integrate UnifiedNLP into this project once it runs with LineageOS 17.1, as the Omnirom prebuilts do not include it.

### Are you going to integrate F-Droid, Aurora, and additional packages into the build?

No, but you can define them in the `LOS_CUSTOM_PACKAGES` variable, assuming all the packages you want to add are defined in the "prebuilt_apks" repo.

In fact, I prefer the approach of having F-Droid, Aurora, etc. installed as user-apps.

### If I disable all features related to microG, what will be the final result?

The final image should be an original LineageOS 17.1 ROM, just signed with your own keys.

### I want to build LOS 17.1 for an unsupported device...

You'll need to define the kernel sources, custom vendor files, and other things by yourself. You can use this project as a starting point, but I don't plan to add support for devices not officially supported by Lineage itself.

### How much time does the build take?

In my experience, the first time I ran it on a DigitalOcean droplet, it took around 9-12 hours from the deploying of the droplet and configuration.

For subsequent builds, using existing resources like downloaded repos and ccached objects, it might take around 2-3 hours.

Meanwhile, the Docker image itself takes around 25-40 minutes to be compiled.

Please consider, though, that these times are based on a deployed instance on DigitalOcean, as I don't have the resources to build LOS on my computer.

### Why does the build process run as root?

I considered running the build process (on the Arch Linux Docker container) as a non-root user, but I haven't tested it yet. In fact I'm sure it will cause some trouble with accessing your keys or the manifest file, for example, considering that the host user runs as root the commands to build/run the Docker container.

TL;DR: it will cause a lot of problems. For now it works as is and everything else does not contribute so much value to this project, unless it implies a big security issue.

### Are you going to replace the LineageOS recovery with TWRP?

No, I will stick to the resources LineageOS provides in their repos. Unfortunately, TWRP has several problems on Android 10 that apparently are not going to be fixed soon because the developer [is not taking care of the project at the moment](https://twrp.me/site/update/2019/10/23/twrp-and-android-10.html). (Note: this refers to devices that *launched* with Android 10.)

I suggest you check out [the TWRP Gerrit](https://gerrit.twrp.me/) from time to time, as this recovery adds so much value to LineageOS and related projects for the end user.

We hope the developers of LOS recovery add support for Nandroid backups in the meantime, but I don't think this is a priority for them.

### Does this Docker image work for LineageOS 16 builds and below?

No, I'm sure it won't work. You might need to install additional packages into the Docker image. But remember, you can always use this project as a starting point to create derivative projects that suit your needs.

## Troubleshooting

### Failed to dial/Permission denied connecting to Docker daemon
```
ERRO[0000] failed to dial gRPC: cannot connect to the Docker daemon. Is 'docker daemon' running on this host?: dial ...
```
```
docker: Got permission denied while trying to connect to the Docker daemon socket at ...
```
* Make sure your Docker daemon/service is running.
* Call `docker build ...` and `docker run ...` with superuser/administrator privileges.

### Problem importing keys
```
:: Importing keys with gpg...
gpg: keyserver receive failed: No keyserver available
problem importing keys
```
This could be because you're behind a proxy. If so, try disabling it and calling `docker build ...` again. You can re-enable the proxy once that command has finished.

### EOFError, Error ocurred when signing APKs
```
/usr/lib/python3.8/getpass.py:91: GetPassWarning: Can not control echo on the terminal.
  passwd = fallback_getpass(prompt, stream)
Warning: Password input may be echoed.
Enter password for /root/los-build/custom_keys/media key> Traceback (most recent call last):
  File "/usr/lib/python3.8/getpass.py", line 69, in unix_getpass
    old = termios.tcgetattr(fd)     # a copy to save
termios.error: (25, 'Inappropriate ioctl for device')

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "./build/tools/releasetools/sign_target_files_apks", line 1227, in <module>
    main(sys.argv[1:])
  File "./build/tools/releasetools/sign_target_files_apks", line 1200, in main
    key_passwords = common.GetKeyPasswords(
  File "/root/los-build/build/make/tools/releasetools/common.py", line 1045, in GetKeyPasswords
    key_passwords.update(PasswordManager().GetPasswords(need_passwords))
  File "/root/los-build/build/make/tools/releasetools/common.py", line 1478, in GetPasswords
    current = self.UpdateAndReadFile(current)
  File "/root/los-build/build/make/tools/releasetools/common.py", line 1499, in UpdateAndReadFile
    return self.PromptResult(current)
  File "/root/los-build/build/make/tools/releasetools/common.py", line 1491, in PromptResult
    result[k] = getpass.getpass(
  File "/usr/lib/python3.8/getpass.py", line 91, in unix_getpass
    passwd = fallback_getpass(prompt, stream)
  File "/usr/lib/python3.8/getpass.py", line 126, in fallback_getpass
    return _raw_input(prompt, stream)
  File "/usr/lib/python3.8/getpass.py", line 148, in _raw_input
    raise EOFError
EOFError
[2020-07-28 20:31:09] Error: Error ocurred when signing APKs
```
The signing script tried to ask for the [passphrase(s)](https://www.ssh.com/ssh/passphrase) for your encryption keys, but it couldn't because you were running the docker image without any connection to a terminal that it could use to do that. Call `docker run...` with the `-i` flag.

### The build failed due to a lack of memory

I suggest tweaking `JAVA_TOOL_OPTIONS`, expanding your swap space, or running the build on a machine with more memory.

### I built and installed the ROM, but microG isn't working very well

As of this writing, the microG community is still working on compatibility with Android 10/Q (LOS 17.1). After several tests I did on my cell phone, I suggest you build your custom image with no microG features, just signature spoofing. Install your custom build on your phone, flash Magisk from [the official Github](https://github.com/topjohnwu/Magisk/releases), and install [MinMicroG](https://github.com/FriendlyNeighborhoodShane/MinMicroG_releases/releases) as a Magisk module.

### My problem isn't listed here; what do I do?!

First, you need a lot of patience to dig into the problem; sometimes it will be on your end and sometimes it will be from the project itself. Please check carefully that you are running the latest version of this Docker image, check that you have all required dependencies installed, check file permissions, etc.

One thing that happened to me is that the build failed several times; I solved it by deleting the `vendor` and `frameworks` folders and running the build process again. Don't worry; it will re-download the files.

In other cases, try to change the values of the environment variables, if you know what you're doing. For example, disable signature spoofing, remove some prebuilt packages, etc.

Also, check your device pipeline on [LineageOS Build pipelines](https://buildkite.com/lineageos/). If something is failing there for your device, surely it will fail with this Docker image, as we draw from the same sources.

Of course, you can always open a new issue.

## Credits and license

Thanks to

- **MicroG** (Marvin and collaborators)
- **LineageOS For MicroG** team (they are the authors of the Docker image that was a start point for this project)
- **SolidHal** (author of patches and improvements to run the builds for LOS 17.1)
- **Omnirom** team (for the prebuilt APKs that run in Android 10)
- **TheMuppets** team (providers of vendor binaries for a lot of devices)
- **FriendlyNeighborhoodShane** and collaborators, for the MinMicroG package which is a fork of MicroG that works on Android 10
- The whole **LineageOS** team, for this fantastic project
- And the (future) contributors of **Minimal Docker for LOS**

>If part of your contributions are directly used by this project, but I didn't mention you, please let me know and I will add you to the list. Thanks for understanding and apologizes for that.

This is an open-source project by **@fremontt** under GNU GPL v3.0
